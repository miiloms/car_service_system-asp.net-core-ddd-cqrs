﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CarService.Data.DataBase.Migrations
{
    public partial class initialize : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CarBrand",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FileImageId = table.Column<long>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarBrand", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ColorSet",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 128, nullable: true),
                    HexCode = table.Column<string>(maxLength: 7, nullable: true),
                    IsMetallic = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ColorSet", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransmissionType",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 128, nullable: true),
                    Name = table.Column<string>(maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransmissionType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TypeOfCar",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 128, nullable: true),
                    Name = table.Column<string>(maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeOfCar", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CarMake",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BrandId = table.Column<long>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarMake", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CarMake_CarBrand_BrandId",
                        column: x => x.BrandId,
                        principalTable: "CarBrand",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CarGeneration",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EndDate = table.Column<DateTime>(nullable: true),
                    MakeId = table.Column<long>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarGeneration", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CarGeneration_CarMake_MakeId",
                        column: x => x.MakeId,
                        principalTable: "CarMake",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CarSeries",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GenerationId = table.Column<long>(nullable: true),
                    MakeId = table.Column<long>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarSeries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CarSeries_CarGeneration_GenerationId",
                        column: x => x.GenerationId,
                        principalTable: "CarGeneration",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CarSeries_CarMake_MakeId",
                        column: x => x.MakeId,
                        principalTable: "CarMake",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CarModification",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EndProduction = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: true),
                    SeriesId = table.Column<long>(nullable: false),
                    StartProduction = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarModification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CarModification_CarSeries_SeriesId",
                        column: x => x.SeriesId,
                        principalTable: "CarSeries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Car",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Comment = table.Column<string>(nullable: true),
                    ModificationCarId = table.Column<long>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true),
                    Vin = table.Column<string>(maxLength: 17, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Car", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Car_CarModification_ModificationCarId",
                        column: x => x.ModificationCarId,
                        principalTable: "CarModification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DataSheet",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(maxLength: 255, nullable: true),
                    CarId = table.Column<long>(nullable: false),
                    ColorSetId = table.Column<long>(nullable: true),
                    DateOfCreation = table.Column<DateTime>(nullable: true),
                    DateOfModification = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false, defaultValue: true),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    IssueDate = table.Column<DateTime>(nullable: true),
                    LicensePlate = table.Column<string>(maxLength: 50, nullable: true),
                    Number = table.Column<string>(maxLength: 50, nullable: false),
                    OwnerMiddleName = table.Column<string>(maxLength: 128, nullable: true),
                    OwnerName = table.Column<string>(maxLength: 128, nullable: true),
                    OwnerSerName = table.Column<string>(maxLength: 128, nullable: true),
                    TransmissionTypeId = table.Column<long>(nullable: false),
                    TypeOfCarId = table.Column<long>(nullable: true),
                    WeightMax = table.Column<int>(nullable: true),
                    WeightUnladen = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataSheet", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DataSheet_Car_CarId",
                        column: x => x.CarId,
                        principalTable: "Car",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DataSheet_ColorSet_ColorSetId",
                        column: x => x.ColorSetId,
                        principalTable: "ColorSet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DataSheet_TransmissionType_TransmissionTypeId",
                        column: x => x.TransmissionTypeId,
                        principalTable: "TransmissionType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DataSheet_TypeOfCar_TypeOfCarId",
                        column: x => x.TypeOfCarId,
                        principalTable: "TypeOfCar",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Car_ModificationCarId",
                table: "Car",
                column: "ModificationCarId");

            migrationBuilder.CreateIndex(
                name: "IX_Car_Vin",
                table: "Car",
                column: "Vin",
                unique: true,
                filter: "[Vin] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_CarGeneration_MakeId",
                table: "CarGeneration",
                column: "MakeId");

            migrationBuilder.CreateIndex(
                name: "IX_CarMake_BrandId",
                table: "CarMake",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_CarModification_SeriesId",
                table: "CarModification",
                column: "SeriesId");

            migrationBuilder.CreateIndex(
                name: "IX_CarSeries_GenerationId",
                table: "CarSeries",
                column: "GenerationId");

            migrationBuilder.CreateIndex(
                name: "IX_CarSeries_MakeId",
                table: "CarSeries",
                column: "MakeId");

            migrationBuilder.CreateIndex(
                name: "IX_DataSheet_CarId",
                table: "DataSheet",
                column: "CarId");

            migrationBuilder.CreateIndex(
                name: "IX_DataSheet_ColorSetId",
                table: "DataSheet",
                column: "ColorSetId");

            migrationBuilder.CreateIndex(
                name: "IX_DataSheet_TransmissionTypeId",
                table: "DataSheet",
                column: "TransmissionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DataSheet_TypeOfCarId",
                table: "DataSheet",
                column: "TypeOfCarId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DataSheet");

            migrationBuilder.DropTable(
                name: "Car");

            migrationBuilder.DropTable(
                name: "ColorSet");

            migrationBuilder.DropTable(
                name: "TransmissionType");

            migrationBuilder.DropTable(
                name: "TypeOfCar");

            migrationBuilder.DropTable(
                name: "CarModification");

            migrationBuilder.DropTable(
                name: "CarSeries");

            migrationBuilder.DropTable(
                name: "CarGeneration");

            migrationBuilder.DropTable(
                name: "CarMake");

            migrationBuilder.DropTable(
                name: "CarBrand");
        }
    }
}
