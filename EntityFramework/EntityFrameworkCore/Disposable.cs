﻿
namespace CarService.Data.DataBase.EntityFrameworkCore
{
    using System;

    public class Disposable: IDisposable
    {
        private bool _isDisposed;

        ~Disposable()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposing)
        {
            _isDisposed = true;
        }
    }
}
