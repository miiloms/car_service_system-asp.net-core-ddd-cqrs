﻿
namespace CarService.Data.DataBase.EntityFrameworkCore
{
    using System;
    using System.IO;
    using Microsoft.Extensions.Logging;

    public class DataBaseLoggerFactory : ILoggerFactory
    {
        private ILoggerProvider LoggerProvider { set; get; }

        public void AddProvider(ILoggerProvider provider)
        {
            this.LoggerProvider = provider;
        }

        public ILogger CreateLogger(string categoryName)
        {
            return this.LoggerProvider != null 
                ? this.LoggerProvider.CreateLogger(categoryName) 
                : new DataBaseLogger();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
    public class DataBaseLoggerProvider : ILoggerProvider
    {
        public ILogger CreateLogger(string categoryName)
        {
            return new DataBaseLogger();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }

    public class DataBaseLogger : ILogger
    {
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId,
                TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            File.AppendAllText("DataBaseLog.txt", formatter(state, exception));
            Console.WriteLine(formatter(state, exception));
        }
    }
}
