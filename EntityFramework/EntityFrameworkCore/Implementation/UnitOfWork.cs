﻿
using CarService.Data.DataBase.EntityFrameworkCore.Interfaces;

namespace CarService.Data.DataBase.EntityFrameworkCore.Implementation
{
    using System;
    using System.Threading.Tasks;
    using CrossCutting.Validation;

    public class UnitOfWork: Disposable, IUnitOfWork
    {
        public UnitOfWork(IDataBase database)
        {
            Check.Argument.IsNotNull(database);

            _database = database;
        }

        public UnitOfWork(IDataBaseFactory factory)
            : this(factory.Create())
        {
        }

        public virtual void SaveChanges()
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }

            _database.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }

            return _database.SaveChangesAsync();
        }

        protected void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                _isDisposed = true;
            }

            base.Dispose();
        }

        private readonly IDataBase _database;
        private bool _isDisposed;
    }
}
