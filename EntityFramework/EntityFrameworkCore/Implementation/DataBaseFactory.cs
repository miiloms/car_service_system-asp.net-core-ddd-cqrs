﻿
using CarService.Data.DataBase.EntityFrameworkCore.Interfaces;

namespace CarService.Data.DataBase.EntityFrameworkCore.Implementation
{

    using CrossCutting.Validation;

    public class DataBaseFactory : Disposable, IDataBaseFactory
    {
        public DataBaseFactory(string connectionString, IDataBase database)
        {
            Check.Argument.IsNotNull(database);

            _database = database;
        }

        public IDataBase Create()
        {
            return _database;
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _database?.Dispose();
            }

            base.Dispose();
        }

        private readonly IDataBase _database;
    }
}
