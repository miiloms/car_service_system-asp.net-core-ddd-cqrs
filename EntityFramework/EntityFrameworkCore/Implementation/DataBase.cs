﻿
using CarService.Data.DataBase.EntityFrameworkCore.Interfaces;
using CarService.Data.DataBase.EntityFrameworkCore.Setup;

namespace CarService.Data.DataBase.EntityFrameworkCore.Implementation
{
    using System;
    using System.Threading.Tasks;
    using Domain.DbEntities.Implementation.Car;
    using Domain.DbEntities.Implementation.Car.HierarchyOfBrands;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using CarService.CrossCutting.AppSetting;

    public class DataBaseContext : DbContext, IDataBase
    {
        private readonly ILoggerFactory _loggerFactory;
        public DataBaseContext(ILoggerFactory loggerFactory)
        {
            this._loggerFactory = loggerFactory;
        }
        //public DataBase(DbContextOptionsBuilder dbContextBuilder) : base(dbContextBuilder.Options)
        //{
        //    //var type = typeof(Microsoft.EntityFrameworkCore.SqlServerEntityTypeBuilderExtensions);
        //    //Logger.Log.Debug(type.ToString());
        //    ////Configuration.LazyLoadingEnabled = false;
        //    ////base.
        //    //DataBase.Log = sql => System.Diagnostics.Debug.Write(sql);
        //}

        public DbSet<CarDbModel> Cars { get; set; }
        public DbSet<DataSheet> DataSheets { get; set; }
        public DbSet<ColorSetDbModel> ColorSets { get; set; }
        public DbSet<TransmissionTypeDbModel> TransmissionTypes { get; set; }
        public DbSet<TypeOfCarDbModel> TypesOfCars { get; set; }
        public DbSet<BrandDbModel> Brands { get; set; }
        public DbSet<GenerationDbModel> Generations { get; set; }
        public DbSet<MakeDbModel> Make { get; set; }
        public DbSet<ModificationDbModel> Modifications { get; set; }
        public DbSet<SeriesDbModel> Series { get; set; }

        public int ExecuteStoreProcedure(string query, params object[] parameters)
        {
            return base.Database.ExecuteSqlCommand(query, parameters);
        }

        void IUnitOfWork.SaveChanges()
        {
            try
            {
                base.SaveChanges();
            }
            catch(InvalidOperationException exeption)
            {
                //TODO: LOG
                throw;
            }
            catch(Exception exeption)
            {
                //TODO:LOG
                throw;
            }
        }

        Task<int> IUnitOfWork.SaveChangesAsync()
        {
            try
            {
                return base.SaveChangesAsync();
            }
            catch (InvalidOperationException exeption)
            {
                //TODO:LOG
                throw;
            }
            catch (Exception exeption)
            {
                //TODO:LOG
                throw;
            }
        }

        public void SetEntityState<TEntity>(TEntity entity, EntityState entityState) where TEntity : class
        {
            this.Entry(entity).State = entityState;
        }

        public DbSet<TEntity> GetDbSet<TEntity>()
            where TEntity : class
        {
            return Set<TEntity>();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(AppSettings.ConnectionString);
            optionsBuilder.UseLoggerFactory(this._loggerFactory);
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            DataBaseSetupManager.Setup(modelBuilder);
        }
    }
}
