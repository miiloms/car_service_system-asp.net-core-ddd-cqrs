﻿
namespace CarService.Data.DataBase.EntityFrameworkCore.Setup.Entities
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public abstract class BaseEntitySetupManager<TEntity> where TEntity: class
    {
        private readonly ModelBuilder modelBuilder;

        public EntityTypeBuilder<TEntity> Entity => this.modelBuilder.Entity<TEntity>();

        protected BaseEntitySetupManager(ModelBuilder modelBuilder)
        {
            this.modelBuilder = modelBuilder;
        }

        public abstract void Setup();
    }
}
