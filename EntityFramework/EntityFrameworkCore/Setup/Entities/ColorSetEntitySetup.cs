﻿
namespace CarService.Data.DataBase.EntityFrameworkCore.Setup.Entities
{
    using Domain.DbEntities.Implementation.Car;
    using Microsoft.EntityFrameworkCore;

    public class ColorSetEntitySetup : BaseEntitySetupManager<ColorSetDbModel>
    {
        public ColorSetEntitySetup(ModelBuilder modelBuilder) : base(modelBuilder){ }

        public override void Setup()
        {
            Entity.ToTable("ColorSet");
            Entity.HasKey(cs => cs.Id);
            Entity.Property(cs => cs.Id);
            Entity.Property(cs => cs.Name).HasMaxLength(128);
            Entity.Property(cs => cs.Description).HasMaxLength(128);
            Entity.Property(cs => cs.HexCode).HasMaxLength(7);
            Entity.Property(cs => cs.IsMetallic);
        }
    }
}
