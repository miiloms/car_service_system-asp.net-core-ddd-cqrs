﻿
namespace CarService.Data.DataBase.EntityFrameworkCore.Setup.Entities
{
    using Domain.DbEntities.Implementation.Car;
    using Microsoft.EntityFrameworkCore;

    public class TransmissionTypeEntitySetup : BaseEntitySetupManager<TransmissionTypeDbModel>
    {
        public TransmissionTypeEntitySetup(ModelBuilder modelBuilder) : base(modelBuilder) { }

        public override void Setup()
        {
            Entity.ToTable("TransmissionType");
            Entity.HasKey(tt => tt.Id);
            Entity.Property(tt => tt.Id);
            Entity.Property(tt => tt.Name).HasMaxLength(128);
            Entity.Property(tt => tt.Description).HasMaxLength(128);
        }
    }
}
