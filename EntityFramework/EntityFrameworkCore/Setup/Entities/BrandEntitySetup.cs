﻿
namespace CarService.Data.DataBase.EntityFrameworkCore.Setup.Entities
{
    using Domain.DbEntities.Implementation.Car.HierarchyOfBrands;
    using Microsoft.EntityFrameworkCore;

    public class BrandEntitySetup : BaseEntitySetupManager<BrandDbModel>
    {
        public BrandEntitySetup(ModelBuilder modelBuilder) : base(modelBuilder){}

        public override void Setup()
        {
            Entity.ToTable("CarBrand");
            Entity.HasKey(b => b.Id);
            Entity.Property(b => b.Id);
            Entity.Property(b => b.Name);
            Entity.Property(b => b.FileImageId);

            Entity.HasMany(b => b.Makes)
                .WithOne(mk => mk.Brand)
                .HasForeignKey(mk => mk.BrandId);
        }
    }
}
