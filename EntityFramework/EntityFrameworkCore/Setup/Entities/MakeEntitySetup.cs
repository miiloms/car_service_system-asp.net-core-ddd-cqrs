﻿
namespace CarService.Data.DataBase.EntityFrameworkCore.Setup.Entities
{
    using Domain.DbEntities.Implementation.Car.HierarchyOfBrands;
    using Microsoft.EntityFrameworkCore;

    public class MakeEntitySetup : BaseEntitySetupManager<MakeDbModel>
    {
        public MakeEntitySetup(ModelBuilder modelBuilder) : base(modelBuilder){}

        public override void Setup()
        {
            Entity.ToTable("CarMake");
            Entity.HasKey(mk => mk.Id);
            Entity.Property(mk => mk.Id);
            Entity.Property(mk => mk.Name);
            Entity.Property(mk => mk.BrandId);

            Entity.HasMany(mk => mk.Generations)
                .WithOne(g => g.Make)
                .HasForeignKey(g => g.MakeId);

            Entity.HasMany(mk => mk.Series)
                .WithOne(s => s.Make)
                .HasForeignKey(s => s.MakeId);
        }
    }
}
