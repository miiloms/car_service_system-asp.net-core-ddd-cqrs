﻿
namespace CarService.Data.DataBase.EntityFrameworkCore.Setup.Entities
{
    using Domain.DbEntities.Implementation.Car;
    using Microsoft.EntityFrameworkCore;

    public class TypeOfCarEntitySetup : BaseEntitySetupManager<TypeOfCarDbModel>
    {
        public TypeOfCarEntitySetup(ModelBuilder modelBuilder) : base(modelBuilder) { }

        public override void Setup()
        {
            Entity.ToTable("TypeOfCar");
            Entity.HasKey(tc => tc.Id);
            Entity.Property(tc => tc.Id);
            Entity.Property(tc => tc.Name).HasMaxLength(128);
            Entity.Property(tc => tc.Description).HasMaxLength(128);

            Entity.HasMany(tc => tc.DataSheets)
                .WithOne(ds => ds.TypeOfCar)
                .IsRequired(false);
        }
    }
}
