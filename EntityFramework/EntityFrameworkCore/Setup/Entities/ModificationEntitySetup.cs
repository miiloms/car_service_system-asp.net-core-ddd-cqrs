﻿
namespace CarService.Data.DataBase.EntityFrameworkCore.Setup.Entities
{
    using Domain.DbEntities.Implementation.Car.HierarchyOfBrands;
    using Microsoft.EntityFrameworkCore;

    public class ModificationEntitySetup : BaseEntitySetupManager<ModificationDbModel>
    {
        public ModificationEntitySetup(ModelBuilder modelBuilder) : base(modelBuilder){}

        public override void Setup()
        {
            Entity.ToTable("CarModification");
            Entity.HasKey(m => m.Id);
            Entity.Property(m => m.Id);
            Entity.Property(m => m.Name).HasMaxLength(255);
            Entity.Property(m => m.StartProduction);
            Entity.Property(m => m.EndProduction);
            Entity.Property(m => m.SeriesId).IsRequired();

            Entity.HasMany(m => m.Cars)
                .WithOne(c => c.Modification)
                .HasForeignKey(c => c.ModificationCarId);
        }
    }
}
