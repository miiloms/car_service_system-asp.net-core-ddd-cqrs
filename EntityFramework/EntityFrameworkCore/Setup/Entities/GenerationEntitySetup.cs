﻿
namespace CarService.Data.DataBase.EntityFrameworkCore.Setup.Entities
{
    using Domain.DbEntities.Implementation.Car.HierarchyOfBrands;
    using Microsoft.EntityFrameworkCore;

    public class GenerationEntitySetup : BaseEntitySetupManager<GenerationDbModel>
    {
        public GenerationEntitySetup(ModelBuilder modelBuilder) : base(modelBuilder) { }

        public override void Setup()
        {
            Entity.ToTable("CarGeneration");
            Entity.HasKey(g => g.Id);
            Entity.Property(g => g.Id);
            Entity.Property(g => g.Name).HasMaxLength(255); ;
            Entity.Property(g => g.MakeId).IsRequired();
            Entity.Property(g => g.StartDate);
            Entity.Property(g => g.EndDate);

            Entity.HasMany(g => g.Series)
                .WithOne(s => s.Generation)
                .HasForeignKey(s => s.GenerationId);

        }
    }
}
