﻿namespace CarService.Data.DataBase.EntityFrameworkCore.Setup.Entities
{
    using Domain.DbEntities.Implementation.Car;
    using Microsoft.EntityFrameworkCore;

    public class CarEntitySetup : BaseEntitySetupManager<CarDbModel>
    {
        public CarEntitySetup(ModelBuilder modelBuilder) : base(modelBuilder){}

        public override void Setup()
        {
            Entity.ToTable("Car");
            Entity.HasKey(c => c.Id);
            Entity.Property(c => c.Id);
            Entity.Property(c => c.Vin).HasMaxLength(17);
            Entity.Property(c => c.ModificationCarId);
            Entity.Property(c => c.Comment);
            Entity.Property(c => c.RowVersion).IsRowVersion();

            Entity.HasIndex(c => c.Vin).IsUnique();

            Entity.HasMany(c => c.DataSheets)
                .WithOne(ds => ds.Car)
                .HasForeignKey(ds => ds.CarId);

        }
    }
}
