﻿
namespace CarService.Data.DataBase.EntityFrameworkCore.Setup.Entities
{
    using Domain.DbEntities.Implementation.Car.HierarchyOfBrands;
    using Microsoft.EntityFrameworkCore;

    public class SeriesEntitySetup : BaseEntitySetupManager<SeriesDbModel>
    {
        public SeriesEntitySetup(ModelBuilder modelBuilder) : base(modelBuilder){}

        public override void Setup()
        {
            Entity.ToTable("CarSeries");
            Entity.HasKey(s => s.Id);
            Entity.Property(s => s.Id);
            Entity.Property(s => s.GenerationId);
            Entity.Property(s => s.MakeId).IsRequired();
            Entity.Property(s => s.Name).HasMaxLength(255);

            Entity.HasMany(s => s.Modifications)
                .WithOne(s => s.Series)
                .HasForeignKey(m => m.SeriesId);

        }
    }
}
