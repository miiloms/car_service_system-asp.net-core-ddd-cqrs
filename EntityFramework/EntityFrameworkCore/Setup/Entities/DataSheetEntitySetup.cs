﻿namespace CarService.Data.DataBase.EntityFrameworkCore.Setup.Entities
{
    using Domain.DbEntities.Implementation.Car;
    using Microsoft.EntityFrameworkCore;

    class DataSheetEntitySetup : BaseEntitySetupManager<DataSheet>
    {
        public DataSheetEntitySetup(ModelBuilder modelBuilder) : base(modelBuilder){}

        public override void Setup()
        {
            Entity.ToTable("DataSheet");
            Entity.HasKey(ds => ds.Id);
            Entity.Property(ds => ds.Id);
            Entity.Property(ds => ds.WeightUnladen);
            Entity.Property(ds => ds.WeightMax);
            Entity.Property(ds => ds.CarId).IsRequired();
            //Entity.Property(ds => ds.TypeOfCarId);
            Entity.Property(ds => ds.TransmissionTypeId);
            Entity.Property(ds => ds.OwnerSerName).HasMaxLength(128);
            Entity.Property(ds => ds.OwnerName).HasMaxLength(128);
            Entity.Property(ds => ds.OwnerMiddleName).HasMaxLength(128);
            Entity.Property(ds => ds.Number).IsRequired().HasMaxLength(50);
            Entity.Property(ds => ds.LicensePlate).HasMaxLength(50);
            Entity.Property(ds => ds.IssueDate);
            Entity.Property(ds => ds.IsDeleted).IsRequired().HasDefaultValue(false);
            Entity.Property(ds => ds.IsActive).IsRequired().HasDefaultValue(true);
            //Entity.Property(ds => ds.ColorId).IsRequired();
            Entity.Property(ds => ds.Address).HasMaxLength(255);
            Entity.Property(ds => ds.DateOfCreation);
            Entity.Property(ds => ds.DateOfModification);

            Entity.HasOne(ds => ds.ColorSet)
                .WithMany(cs => cs.DataSheets)
                .IsRequired(false);

            Entity.HasOne(ds => ds.TypeOfCar)
                .WithMany(tp => tp.DataSheets)
                .IsRequired(false);
        }
    }
}
