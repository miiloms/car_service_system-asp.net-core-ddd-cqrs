﻿
namespace CarService.Data.DataBase.EntityFrameworkCore.Setup
{

    using Entities;
    using Microsoft.EntityFrameworkCore;

    public  static class DataBaseSetupManager
    {
        public static void Setup(ModelBuilder modelBuilder)
        {
            new CarEntitySetup(modelBuilder).Setup();
            new DataSheetEntitySetup(modelBuilder).Setup();
            new ModificationEntitySetup(modelBuilder).Setup();
            new GenerationEntitySetup(modelBuilder).Setup();
            new MakeEntitySetup(modelBuilder).Setup();
            new SeriesEntitySetup(modelBuilder).Setup();
            new BrandEntitySetup(modelBuilder).Setup();
            new ColorSetEntitySetup(modelBuilder).Setup();
            new TransmissionTypeEntitySetup(modelBuilder).Setup();
            new TypeOfCarEntitySetup(modelBuilder).Setup();
        }
    }
}
