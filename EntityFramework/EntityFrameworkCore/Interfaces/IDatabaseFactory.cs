﻿
namespace CarService.Data.DataBase.EntityFrameworkCore.Interfaces
{
    using System;

    public interface IDataBaseFactory : IDisposable
    {
		IDataBase Create();
    }
}
