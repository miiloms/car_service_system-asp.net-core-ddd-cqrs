﻿
namespace CarService.Data.DataBase.EntityFrameworkCore.Interfaces
{
    using Domain.DbEntities.Implementation.Car;
    using Domain.DbEntities.Implementation.Car.HierarchyOfBrands;
    using Microsoft.EntityFrameworkCore;

    public interface IDataBase: IUnitOfWork
    {
        #region DbSets
        DbSet<CarDbModel> Cars { get; set; }
        DbSet<DataSheet> DataSheets { get; set; }
        DbSet<ColorSetDbModel> ColorSets { get; set; }
        DbSet<TransmissionTypeDbModel> TransmissionTypes { get; set; }
        DbSet<TypeOfCarDbModel> TypesOfCars { get; set; }
        DbSet<BrandDbModel> Brands { get; set; }
        DbSet<GenerationDbModel> Generations { get; set; }
        DbSet<MakeDbModel> Make { get; set; }
        DbSet<ModificationDbModel> Modifications { get; set; }
        DbSet<SeriesDbModel> Series { get; set; }
        #endregion

        DbSet<TEntity> GetDbSet<TEntity>()
            where TEntity : class;

        void SetEntityState<TEntity>(TEntity entity, EntityState entityState) where TEntity : class;

        int ExecuteStoreProcedure(string query, params object[] parameters);
    }
}
