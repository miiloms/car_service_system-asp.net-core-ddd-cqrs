﻿
namespace CarService.Data.DataBase.EntityFrameworkCore.Interfaces
{
    using System;
    using System.Threading.Tasks;

    public interface IUnitOfWork: IDisposable
    {
        void SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
