﻿
namespace CarService.Tests.UnitTests
{
    using System.Collections.Generic;
    using System.Linq;
    using Domain.DbEntities.Implementation.Car;
    using Domain.Repositories.Interfaces.Car;
    using Moq;
    using Xunit;

    public class CarRepositoryTest
    {
        [Fact]
        public void RepositoryReturnListOfCars()
        {
            //Arrange
            var moq = new Mock<ICarRepository>();
            moq.Setup(rep => rep.GetAll()).Returns(GetSetOfCars());
            //Action
            var result = moq.Object.GetAll();
            //Assert
            var res = Assert.IsType<List<CarDbModel>>(result);
            Assert.Equal(GetSetOfCars().Count(), res.Count());
        }

        private IEnumerable<CarDbModel> GetSetOfCars()
        {
            return new List<CarDbModel>
            {
                new CarDbModel {Id = 1, Vin = "FGHDB54521DFD", ModificationCarId = 1052 },
                new CarDbModel {Id = 2, Vin = "FGHDB54521DFD", ModificationCarId = 1520 },
                new CarDbModel {Id = 3, Vin = "FGHDB54521DFD", ModificationCarId = 1555 },
                new CarDbModel {Id = 4, Vin = "FGHDB54521DFD", ModificationCarId = 1965 }
            };
        }
    }
}
