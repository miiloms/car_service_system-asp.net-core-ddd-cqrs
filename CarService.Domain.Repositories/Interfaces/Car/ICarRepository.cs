﻿
namespace CarService.Domain.Repositories.Interfaces.Car
{
    using Base;
    using System.Collections.Generic;
    using DbEntities.Implementation.Car;

    public interface ICarRepository : IRepositoryWithId<CarDbModel>
    {
        CarDbModel GetByVin(string vin);
        CarDbModel GetByDataSheetNumber(string dataSheetNumber);
        IEnumerable<CarDbModel> GetByListViNs(string[] vins);
    }
}
