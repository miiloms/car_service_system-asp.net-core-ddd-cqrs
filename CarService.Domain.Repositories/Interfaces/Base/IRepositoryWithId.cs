﻿
namespace CarService.Domain.Repositories.Interfaces.Base
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IRepositoryWithId<TEntity>: IRepository<TEntity>
    {
        TEntity GetById(long id);
        Task<TEntity> GetByIdAsync(long id);
        bool Exists(long id);
        void RemoveListByIds(long[] ids);
        void RemoveById(long id);
        Task RemoveByIdAsync(long id);
        IEnumerable<TEntity> SelectListByIds(long[] ids);
    }
}
