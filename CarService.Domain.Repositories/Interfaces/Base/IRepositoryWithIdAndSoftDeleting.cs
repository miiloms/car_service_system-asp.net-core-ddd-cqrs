﻿
namespace CarService.Domain.Repositories.Interfaces.Base
{
    using DbEntities.Interfaces.Base;

    public interface IRepositoryWithIdAndSoftDeleting<TEntity>: IRepositoryWithId<TEntity>
        where TEntity: class, IEntitySoftRemovable
    {
        void RemoveSoftly(TEntity entity);
    }
}
