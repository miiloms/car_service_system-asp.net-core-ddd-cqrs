﻿
namespace CarService.Domain.Repositories.Interfaces.Base
{
    using System.Collections.Generic;

    public interface IRepository<TEntity>
    {
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void UpdateList(IEnumerable<TEntity> entites);
        void DeleteList(IEnumerable<TEntity> entities);
        IEnumerable<TEntity> GetAll();

    }
}
