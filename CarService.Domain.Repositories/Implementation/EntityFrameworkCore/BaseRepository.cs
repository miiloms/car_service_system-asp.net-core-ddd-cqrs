﻿

namespace CarService.Domain.Repositories.Implementation.EntityFrameworkCore
{
    using Interfaces.Base;
    using CarService.Data.DataBase.EntityFrameworkCore.Interfaces;
    using CrossCutting.Validation;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;

    public abstract class BaseRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        protected IDataBase DataBase;

        protected BaseRepository(IDataBase dataBase)
        {
            Check.Argument.IsNotNull(dataBase);
            this.DataBase = dataBase;
        }

        protected BaseRepository(IDataBaseFactory factory)
            : this(factory.Create())
        {}

        protected internal IDataBase Database => this.DataBase;
        protected DbSet<TEntity> DefaultSet => Database.GetDbSet<TEntity>();
        protected virtual string TableName => typeof(TEntity).Name;


        public void Add(TEntity entity)
        {
            Check.Argument.IsNotNull(entity);
            Database.SetEntityState(entity, EntityState.Added);
        }

        public void Delete(TEntity entity)
        {
            Check.Argument.IsNotNull(entity);
            Database.SetEntityState(entity, EntityState.Deleted);
        }

        public void DeleteList(IEnumerable<TEntity> entities)
        {
            Check.Argument.IsNotNull(entities);

            Parallel.ForEach(entities, (entity) => Database.SetEntityState(entity, EntityState.Deleted));

        }

        public IEnumerable<TEntity> GetAll()
        {
            return DefaultSet.AsEnumerable();
        }

        public void Update(TEntity entity)
        {
            Check.Argument.IsNotNull(entity);

            Database.SetEntityState(entity, EntityState.Modified);
        }

        public void UpdateList(IEnumerable<TEntity> entities)
        {
            Check.Argument.IsNotNull(entities);

            Parallel.ForEach(entities, (entity) => Database.SetEntityState(entity, EntityState.Modified));
        }

    }
}
