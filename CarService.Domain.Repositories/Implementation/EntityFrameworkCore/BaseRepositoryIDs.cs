﻿
namespace CarService.Domain.Repositories.Implementation.EntityFrameworkCore
{
    using Interfaces.Base;
    using DbEntities.Interfaces.Base;
    using Data.DataBase.EntityFrameworkCore.Interfaces;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Linq;
    using System.Text;
    using System.Globalization;

    public abstract class BaseRepositoryIDs<TEntity> : BaseRepository<TEntity>, IRepositoryWithId<TEntity>
        where TEntity:class, IEntityBaseIDs  
    {
        protected BaseRepositoryIDs(IDataBase dataBase): base( dataBase)
        {}

        public virtual bool Exists(long id)
        {
           return  base.DefaultSet.SingleOrDefault(entity => entity.Id == id) !=null;
        }

        public TEntity GetById(long id)
        {
            return DefaultSet.SingleOrDefault(entity => entity.Id == id);
        }

        public Task<TEntity> GetByIdAsync(long id)
        {
            return DefaultSet.FindAsync(id);
        }

        public void RemoveById(long id)
        {
            TEntity entity = this.GetById(id);
            DefaultSet.Remove(entity);
        }

        public async Task RemoveByIdAsync(long id)
        {
            TEntity entity = await this.GetByIdAsync(id);
            DefaultSet.Remove(entity);
        }

        public void RemoveListByIds(long[] ids)
        {
            Database.ExecuteStoreProcedure(this.BuildDeleteByIdsScript(ids));
        }

        protected virtual string BuildDeleteByIdsScript(long[] ids)
        {
            if (ids == null || ids.Length == 0)
            {
                return null;
            }

            const string deleteByIdsQueryTemplate = "DELETE FROM {0} WHERE ID IN ({1});";

            var stringBuilder = new StringBuilder();

            foreach (long item in ids)
            {
                stringBuilder.Append(item.ToString(CultureInfo.InvariantCulture));
                stringBuilder.Append(",");
            }
            string idsStr = stringBuilder.ToString();
            string deleteByIdsScript = string.Format(deleteByIdsQueryTemplate, base.TableName, idsStr.Substring(0, idsStr.Length - 1));
            return deleteByIdsScript;
        }

        public IEnumerable<TEntity> SelectListByIds(long[] ids)
        {
            return DefaultSet.Where(entity => ids.Contains(entity.Id)).AsEnumerable();
        }
    }
}
