﻿
namespace CarService.Domain.Repositories.Implementation.EntityFrameworkCore.Car
{
    using EntityFrameworkCore;
    using CarService.Data.DataBase.EntityFrameworkCore.Interfaces;
    using Interfaces.Car;
    using System.Linq;
    using System.Collections.Generic;
    using DbEntities.Implementation.Car;

    public class CarRepository : BaseRepositoryIDs<CarDbModel>, ICarRepository
    {
        public CarRepository(IDataBase dataBase) : base(dataBase){}

        public CarDbModel GetByDataSheetNumber(string dataSheetNumber)
        {
            return DefaultSet.SingleOrDefault(car => car.DataSheets.Any(ds => ds.Number == dataSheetNumber));
        }

        public IEnumerable<CarDbModel> GetByListViNs(string[] vins)
        {
            return DefaultSet.Where(car => vins.Contains(car.Vin)).ToList();
        }

        public CarDbModel GetByVin(string vin)
        {
            return DefaultSet.SingleOrDefault(car => car.Vin == vin);
        }
    }
}
