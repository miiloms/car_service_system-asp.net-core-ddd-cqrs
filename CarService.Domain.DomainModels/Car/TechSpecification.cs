﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarService.Domain.DomainModels.Car
{
    public class TechSpecification
    {
        public TransmissionType TransmissionType { get; private set; }
        public TypeOfCar TypeOfCar { get; set; }
        public ColorBody Color { get; set; }
        public int? WeightMax { get; private set; }
        public int? WeightUnladen { get; private set; }
    }
}
