﻿
namespace CarService.Domain.DomainModels.Car
{
    using CarService.Domain.DomainExceptions.Car;
    using CarService.Domain.DomainModels.Base;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class Car: IAggregateRoot
    {
        private Car()
        {}
        public Car(string vin, DataSheet activeDataSheet, NameInformation nameInformation)
        {
            if (vin == null)
                throw new ArgumentNullException(nameof(vin));

            if (Vin.Length != 17)
                throw new CarServiceInvalidVinNumberException(vin);

            Vin = vin;
            ActiveDataSheet = activeDataSheet ?? throw new ArgumentNullException(nameof(activeDataSheet));
            NameInformation = nameInformation ?? throw new ArgumentNullException(nameof(nameInformation));
        }

        public string Vin { get; private set; }
        public DataSheet ActiveDataSheet { set; private get; }
        public NameInformation NameInformation { get; private set; }
        public ReadOnlyCollection<DataSheet> HistoryDataSheets
        {
            get
            {
                return _historyDataSheets.AsReadOnly();
            }
        }

        private List<DataSheet> _historyDataSheets { get; set; }

        public void AttachNewDataSheet(DataSheet dataSheet)
        {
            if(ActiveDataSheet != null)
                AddDataSheetInHistory(ActiveDataSheet);

            ActiveDataSheet = dataSheet ?? throw new ArgumentNullException(nameof(dataSheet));
            ActiveDataSheet.SetAsActive();            
        }

        private void AddDataSheetInHistory(DataSheet dataSheet)
        {
            dataSheet.SetAsNotActive();
            _historyDataSheets.Add(dataSheet);
        }
    }
}
