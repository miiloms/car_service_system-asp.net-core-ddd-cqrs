﻿using CarService.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarService.Domain.DomainModels.Car.HierarchyOfBrands
{
    public class Modification : ValueObject
    {
        public string Name { get; set; }
        public DateTime StartProduction { get; set; }
        public DateTime? EndProduction { get; set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new NotImplementedException();
        }
    }
}
