﻿using CarService.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarService.Domain.DomainModels.Car.HierarchyOfBrands
{
    public class Generation: ValueObject
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new NotImplementedException();
        }
    }
}
