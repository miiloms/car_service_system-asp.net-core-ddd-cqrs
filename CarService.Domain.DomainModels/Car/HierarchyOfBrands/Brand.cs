﻿using CarService.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarService.Domain.DomainModels.Car.HierarchyOfBrands
{
    public class Brand: ValueObject
    {
        public string Name { get; set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new NotImplementedException();
        }
    }
}
