﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarService.Domain.DomainModels.Car
{
    public class DataSheet
    {
        private DataSheet()
        {}

        public DataSheet(string number, string licensePlate, Owner carOwner, TechSpecification carTechInformation, DateTime issueDate)
        {
            Number = string.IsNullOrEmpty(number) ? throw new ArgumentNullException(nameof(number)) : number;
            LicensePlate = licensePlate ?? throw new ArgumentNullException(nameof(licensePlate));
            CarOwner = carOwner ?? throw new ArgumentNullException(nameof(carOwner));
            CarTechInformation = carTechInformation ?? throw new ArgumentNullException(nameof(carTechInformation));

            SetAsNotActive();
        }

        public string Number { get; private set; }
        public string LicensePlate { get; private set; }
        public Owner CarOwner { get; private set; }
        public TechSpecification CarTechInformation { get; private set; }
        public DateTime IssueDate { get; private set; }
        public bool IsActive { get; private set; }               

        public virtual void SetAsActive()
        {
            IsActive = true;
        }

        public virtual void SetAsNotActive()
        {
            IsActive = false;
        }
    }
}
