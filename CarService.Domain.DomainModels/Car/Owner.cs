﻿using CarService.Domain.DomainModels.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarService.Domain.DomainModels.Car
{
    public class Owner : Person
    {
        public Owner(Owner owner)
        {
            Name = owner.Name;
            SecondName = owner.SecondName;
            MiddleName = owner.MiddleName;
            DateOfBirth = owner.DateOfBirth;
            Sex = owner.Sex;
            Address = owner.Address;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Name;
            yield return SecondName;
            yield return MiddleName;
            yield return DateOfBirth;
            yield return Sex;
            yield return Address;
        }
    }
}
