﻿using CarService.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarService.Domain.DomainModels.Car
{
    public class TypeOfCar: ValueObject
    {
        public string Name { get; private set; }
        public string Description { get; private set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Name;
            yield return Description;
        }
    }
}
