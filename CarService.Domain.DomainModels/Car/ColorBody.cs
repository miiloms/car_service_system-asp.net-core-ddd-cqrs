﻿using CarService.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarService.Domain.DomainModels.Car
{
    public class ColorBody: ValueObject
    {
        private ColorBody()
        {}

        public ColorBody(string description, string hexCode, bool isMetallic)
        {
            Description = string.IsNullOrEmpty(description) 
                ? throw new ArgumentNullException(nameof(description))
                : description;
            hexCode = string.IsNullOrEmpty(hexCode)
                ? throw new ArgumentNullException(nameof(hexCode))
                : hexCode;
        }
        public string Description { get; private set; }
        public string HexCode { get; private set; }
        public bool IsMetallic { set; private get; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new NotImplementedException();
        }
    }
}
