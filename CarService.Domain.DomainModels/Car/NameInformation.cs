﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace CarService.Domain.DomainModels.Car
{
    using CarService.Domain.DomainModels.Car.HierarchyOfBrands;
    public class NameInformation
    {
        private NameInformation()
        {}

        public NameInformation(Brand brand, Generation generation, Series series, Modification modification)
        {
            Brand = brand ?? throw new ArgumentNullException(nameof(brand));
            Generation = generation ?? throw new ArgumentNullException(nameof(generation));
            Series = series ?? throw new ArgumentNullException(nameof(series));
            Modification = modification ?? throw new ArgumentNullException(nameof(modification));
        }
        public Brand Brand { get; private set; }
        public Generation Generation { get; private set; }
        public Make Make { get; private set; }
        public Series Series { get; private set; }
        public Modification Modification { get; private set; }
    }
}
