﻿namespace CarService.Domain.DomainModels.Common
{
    using CarService.Domain.DomainModels.Base;
    using System;

    public abstract class Person: ValueObject
    {
        public string Name { get; protected set; }
        public string SecondName { get; protected set; }
        public string MiddleName { get; protected set; }
        public DateTime DateOfBirth { get; protected set; } = DateTime.UtcNow;
        public Sex Sex { get; protected set; }
        public Address Address { get; protected set; }

        public int Age { get => DateTime.UtcNow.Year - DateOfBirth.Year; }

    }

    public enum Sex
    {
        None   = 0,
        Male   = 1,
        Female = 2
    }
}
