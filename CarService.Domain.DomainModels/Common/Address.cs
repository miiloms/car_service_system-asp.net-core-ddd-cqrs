﻿using CarService.Domain.DomainModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarService.Domain.DomainModels.Common
{
    public class Address: ValueObject
    {
        public string Country { get; }
        public string City { get; }
        public string Street { get; }
        public string Building { get; }
        public int? Zip { get; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Country;
            yield return City;
            yield return Street;
            yield return Building;
            yield return Zip;
        }
    }
}
