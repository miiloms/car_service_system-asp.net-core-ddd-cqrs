﻿namespace CarService.Domain.DbEntities.Interfaces.Base
{
    public interface IEntityActivable
    {
        bool IsActive { get; set; }
    }
}
