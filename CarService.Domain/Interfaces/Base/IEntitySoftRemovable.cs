﻿namespace CarService.Domain.DbEntities.Interfaces.Base
{
    public interface IEntitySoftRemovable
    {
        bool IsDeleted { get; set; }
    }
}
