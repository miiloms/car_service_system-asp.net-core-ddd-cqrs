﻿namespace CarService.Domain.DbEntities.Interfaces.Base
{
    public interface IEntityNamed
    {
        string Name { get; set; }
    }
}
