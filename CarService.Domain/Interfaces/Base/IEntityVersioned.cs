﻿namespace CarService.Domain.DbEntities.Interfaces.Base
{
    public interface IEntityVersioned
    {
       byte[] RowVersion { get; set; }
    }
}
