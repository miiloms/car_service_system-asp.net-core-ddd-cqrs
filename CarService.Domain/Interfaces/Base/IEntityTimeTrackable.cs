﻿using System;

namespace CarService.Domain.DbEntities.Interfaces.Base
{
    public interface IEntityTimeTrackable
    {
        DateTime? DateOfCreation { get; set; }
        DateTime? DateOfModification { get; set; }
    }
}
