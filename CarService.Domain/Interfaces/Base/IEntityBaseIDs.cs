﻿namespace CarService.Domain.DbEntities.Interfaces.Base
{
    public interface IEntityBaseIDs
    {
        long Id { set; get; }
    }
}
