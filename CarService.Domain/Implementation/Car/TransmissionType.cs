﻿
namespace CarService.Domain.DbEntities.Implementation.Car
{
    using System.Collections.Generic;
    using Base;

    public class TransmissionTypeDbModel : EntityBaseIDsNamed
    {
        public TransmissionTypeDbModel()
        {
            this.DataSheets = new HashSet<DataSheetDbModel>(); 
        }
        public string Description { get; set; }

        public virtual ICollection<DataSheetDbModel> DataSheets {get;set;}
    }
}
