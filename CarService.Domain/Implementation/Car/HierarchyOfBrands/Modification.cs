﻿
namespace CarService.Domain.DbEntities.Implementation.Car.HierarchyOfBrands
{
    using System;
    using System.Collections.Generic;
    using Base;

    public class ModificationDbModel : EntityBaseIDsNamed
    {
        public ModificationDbModel()
        {
            this.Cars = new HashSet<CarDbModel>();
        }
        public DateTime StartProduction { get; set; }
        public DateTime? EndProduction { get; set; }
        public long SeriesId { get; set; }

        public virtual SeriesDbModel Series { get; set; }
        public virtual ICollection<CarDbModel> Cars {get; set;}
    }
}
