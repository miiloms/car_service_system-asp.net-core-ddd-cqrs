﻿
namespace CarService.Domain.DbEntities.Implementation.Car.HierarchyOfBrands
{
    using System.Collections.Generic;
    using Base;

    public class SeriesDbModel: EntityBaseIDsNamed
    {
        public SeriesDbModel()
        {
            this.Modifications = new HashSet<ModificationDbModel>();
        }
        public long MakeId { get; set; }
        public long? GenerationId { get; set; }

        public virtual MakeDbModel Make { get; set; }
        public virtual GenerationDbModel Generation { get; set; }
        public virtual ICollection<ModificationDbModel> Modifications { get; set; }
    }
}
