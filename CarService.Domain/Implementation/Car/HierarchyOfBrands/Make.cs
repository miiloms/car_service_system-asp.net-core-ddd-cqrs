﻿
namespace CarService.Domain.DbEntities.Implementation.Car.HierarchyOfBrands
{
    using System.Collections.Generic;
    using Base;

    public class MakeDbModel: EntityBaseIDsNamed
    {
        public MakeDbModel()
        {
            this.Series = new HashSet<SeriesDbModel>();
            this.Generations = new HashSet<GenerationDbModel>();
        }
        public long BrandId { get; set; }

        public virtual BrandDbModel Brand { set; get; }
        public virtual ICollection<SeriesDbModel> Series { set; get; }
        public virtual ICollection<GenerationDbModel> Generations { set; get; }
    }
}
