﻿
namespace CarService.Domain.DbEntities.Implementation.Car.HierarchyOfBrands
{
    using System.Collections.Generic;
    using Base;

    public class BrandDbModel: EntityBaseIDsNamed
    {
        public long  FileImageId { get; set; }

        public virtual ICollection<MakeDbModel> Makes { get; set; }
    }
}
