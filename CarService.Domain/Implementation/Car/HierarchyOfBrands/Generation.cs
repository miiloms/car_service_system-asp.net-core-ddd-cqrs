﻿
namespace CarService.Domain.DbEntities.Implementation.Car.HierarchyOfBrands
{
    using System;
    using System.Collections.Generic;
    using Base;

    public class GenerationDbModel: EntityBaseIDsNamed
    {
        public GenerationDbModel()
        {
            this.Series = new HashSet<SeriesDbModel>();
        }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public long MakeId { get; set; }
        public virtual MakeDbModel Make { get; set; }

        public virtual ICollection<SeriesDbModel> Series { get; set; }
    }
}
