﻿
namespace CarService.Domain.DbEntities.Implementation.Car
{
    using System.Collections.Generic;
    using Base;

    public class TypeOfCarDbModel: EntityBaseIDsNamed
    {
        public TypeOfCarDbModel()
        {
            this.DataSheets = new HashSet<DataSheetDbModel>();
        }

        #region Mapped Properties
        public string Description { get; set; }
        #endregion

        #region Relations
        public virtual ICollection<DataSheetDbModel> DataSheets { get; set; }
        #endregion
    }
}
