﻿
namespace CarService.Domain.DbEntities.Implementation.Car
{
    using System.Collections.Generic;
    using Base;
    using HierarchyOfBrands;

    public class CarDbModel : EntityBaseIDsVersioned
    {
        public CarDbModel()
        {
            this.DataSheets = new HashSet<DataSheetDbModel>();
        }

        #region Mapped Properties
        public string Vin { get; set; }
        public long ModificationCarId { get; set; }
        public string Comment { set; get; }
        #endregion

        #region Relations
        public virtual ICollection<DataSheetDbModel> DataSheets { get; set; }
        public virtual ModificationDbModel Modification { get; set; }
        #endregion
    }
}
