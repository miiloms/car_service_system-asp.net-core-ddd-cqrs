﻿
using CarService.Domain.DbEntities.Interfaces.Base;

namespace CarService.Domain.DbEntities.Implementation.Car
{
    using System;
    using Base;

    public class DataSheetDbModel : EntityBaseIDsSoftRemovableTimeTrackable, IEntityActivable
    {
        #region Mapped Properties
        public string Number { get; set; }
        public string OwnerName { get; set; }
        public string OwnerSerName { get; set; }
        public string OwnerMiddleName { get; set; }
        public string LicensePlate { get; set; }
        public DateTime? IssueDate { get; set;}
        public string Address { get; set; }
        public int? WeightMax { get; set; }
        public int? WeightUnladen { get; set; }
        //public int? TypeOfCarId { get; set; }
        //public int? ColorId { get; set; }
        public long CarId { get; set; }
        public long TransmissionTypeId { get; set; }
        public bool IsActive { get; set; }
        #endregion

        #region Relations
        public virtual CarDbModel Car { get; set; }
        public virtual ColorSetDbModel ColorSet { get; set; }
        public virtual TypeOfCarDbModel TypeOfCar { get; set; }
        public virtual TransmissionTypeDbModel TranssmissionType { get; set; }

        #endregion
    }
}
