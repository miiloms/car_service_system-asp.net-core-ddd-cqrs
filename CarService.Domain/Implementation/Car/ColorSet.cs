﻿
namespace CarService.Domain.DbEntities.Implementation.Car
{
    using System.Collections.Generic;
    using Base;

    public class ColorSetDbModel: EntityBaseIDsNamed
    {
        public ColorSetDbModel()
        {
            this.DataSheets = new HashSet<DataSheetDbModel>();
        }

        #region Mapped Properties
        public string Description { get; set; }
        public string HexCode { get; set; }
        public bool IsMetallic { set; get; }
        #endregion

        #region Relations
        public virtual ICollection<DataSheetDbModel> DataSheets { get; set; }
        #endregion
    }
}
