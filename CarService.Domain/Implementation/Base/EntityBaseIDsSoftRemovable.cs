﻿
using CarService.Domain.DbEntities.Interfaces.Base;

namespace CarService.Domain.DbEntities.Implementation.Base
{
    /// <summary>
    /// Abstract class inlcudes Id and IsDeleted fields
    /// </summary>
    /// <returns></returns>
    public abstract class EntityBaseIDsSoftRemovable : IEntityBaseIDs,  IEntitySoftRemovable
    {
        public long Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}
