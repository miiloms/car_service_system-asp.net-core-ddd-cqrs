﻿
using CarService.Domain.DbEntities.Interfaces.Base;

namespace CarService.Domain.DbEntities.Implementation.Base
{
    using System;

    /// <inheritdoc cref="EntityBaseIDsNamed" />
    /// <summary>
    /// Abstract class inlcudes Id, Name, DateOfCreation and DateOfModification fields
    /// </summary>
    /// <returns></returns>
    public abstract class EntityBaseIDsNamedTimeTrackable : EntityBaseIDsNamed, IEntityTimeTrackable
    {
        public DateTime? DateOfCreation { get; set; }
        public DateTime? DateOfModification { get; set; }
    }
}
