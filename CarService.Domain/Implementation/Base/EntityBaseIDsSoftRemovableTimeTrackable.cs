﻿
using CarService.Domain.DbEntities.Interfaces.Base;

namespace CarService.Domain.DbEntities.Implementation.Base
{
    using System;

    /// <summary>
    /// Abstract class inlcudes Id,  IsDeleted, DateOfCreation and DateOfModification fields
    /// </summary>
    /// <returns></returns>
    public abstract class EntityBaseIDsSoftRemovableTimeTrackable: IEntityBaseIDs,  IEntitySoftRemovable, IEntityTimeTrackable
    {
        public long Id { get; set; }
        public DateTime? DateOfCreation { get; set; }
        public DateTime? DateOfModification { get; set; }
        public bool IsDeleted { get; set; }
    }
}
