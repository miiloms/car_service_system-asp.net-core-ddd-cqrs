﻿
using CarService.Domain.DbEntities.Interfaces.Base;

namespace CarService.Domain.DbEntities.Implementation.Base
{
    /// <summary>
    /// Abstract class inlcudes Id field
    /// </summary>
    /// <returns></returns>
    public abstract class EntityBaseIDs : IEntityBaseIDs
    {
        public long Id { get; set; }
    }
}
