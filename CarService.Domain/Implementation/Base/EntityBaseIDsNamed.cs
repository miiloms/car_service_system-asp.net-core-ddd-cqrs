﻿
using CarService.Domain.DbEntities.Interfaces.Base;

namespace CarService.Domain.DbEntities.Implementation.Base
{
    /// <summary>
    /// Abstract class inlcudes Id and Name fields
    /// </summary>
    /// <returns></returns>
    public abstract class EntityBaseIDsNamed : EntityBaseIDs, IEntityNamed
    {
        public string Name { get; set; }
    }
}
