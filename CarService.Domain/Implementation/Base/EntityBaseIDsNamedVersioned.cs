﻿
using CarService.Domain.DbEntities.Interfaces.Base;

namespace CarService.Domain.DbEntities.Implementation.Base
{
    /// <summary>
    /// Abstract class inlcudes Id, Name and RowVersion fields
    /// </summary>
    /// <returns></returns>
    public abstract class EntityBaseIDsNamedVersioned : EntityBaseIDsNamed, IEntityVersioned
    {
        public byte[] RowVersion { get; set; }
    }
}
