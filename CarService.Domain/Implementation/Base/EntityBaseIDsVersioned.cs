﻿
using CarService.Domain.DbEntities.Interfaces.Base;

namespace CarService.Domain.DbEntities.Implementation.Base
{
    /// <summary>
    /// Abstract class inlcudes Id and RowVersion fields
    /// </summary>
    /// <returns></returns>
    public abstract class EntityBaseIDsVersioned : EntityBaseIDs, IEntityVersioned
    {
        public byte[] RowVersion { get; set; }
    }
}
