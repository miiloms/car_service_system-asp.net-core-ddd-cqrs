﻿
namespace CarService.DependencyInjection.ServiceCollectionCore
{
    using Microsoft.Extensions.DependencyInjection;
    using CarService.Domain.Repositories.Interfaces.Car;
    using CarService.Domain.Repositories.Implementation.EntityFrameworkCore.Car;
    using Microsoft.Extensions.Logging;
    using CarService.Data.DataBase.EntityFrameworkCore.Implementation;
    using CarService.Data.DataBase.EntityFrameworkCore.Interfaces;


    public class DIRegisterManager
    {
        private IServiceCollection services { get; }

        public  DIRegisterManager(IServiceCollection services)
        {
            this.services = services;
        }

        public void RegisterDependencies()
        {
            this.services
                .AddScoped<IDataBase, DataBaseContext>()
                .AddScoped<IDataBaseFactory, DataBaseFactory>()
                .AddScoped<IUnitOfWork, UnitOfWork>()
                .AddTransient<ICarRepository, CarRepository>()
                .AddSingleton<ILoggerFactory, LoggerFactory>();

        }
    }
}
