﻿
namespace CarService.Domain.DomainExceptions.Car
{
    using System;

    [Serializable]
    public class CarServiceInvalidVinNumberException : Exception
    {
        public string Vin { get; private set; }

        public CarServiceInvalidVinNumberException()
        {}
        public CarServiceInvalidVinNumberException(string vin)
            :base($"Invalid vin number of car: {vin}. Length should be 17 characters")
        {
            Vin = vin;
        }
    }
}
