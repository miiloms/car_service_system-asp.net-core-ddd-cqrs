﻿
namespace CarService.CrossCutting.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Diagnostics;
    public class Check
    {
        public static class Argument
        {
            [DebuggerStepThrough]
            public static void IsNotNull(object argument)
            {
                if (argument == null)
                {
                    throw new ArgumentNullException(nameof(argument));
                }
            }
        }
    }
}
