﻿
namespace CarService.CrossCutting.AppSetting
{
    using Microsoft.Extensions.Configuration;
    using System.IO;
    public static class AppSettings
    {
        private static IConfigurationRoot configurations;
        static AppSettings()
        {
            ConfigurationBuilder configBuilder = new ConfigurationBuilder();
            configBuilder.SetBasePath(Directory.GetCurrentDirectory());
            configBuilder.AddJsonFile("appsettings.json");
            configurations = configBuilder.Build();
        }

        public static string ConnectionString => configurations["ConnectionStrings:MainSqlConnection"];
        public static bool IsDebug => bool.TryParse(configurations["IsDebug"], out bool isDebug) && isDebug;
    }
}
